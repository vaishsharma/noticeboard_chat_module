package com.example.lenovopc.chatapp10.model;


import android.graphics.Bitmap;

import java.io.Serializable;

public class ContactsRoom implements Serializable {
    String name, mobile, user_id;
    Bitmap photo;
    private boolean isSelected;

    public ContactsRoom() {

    }
    public String getId(){
        return user_id;
    }

    public void setId(String user_id){

        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }


}