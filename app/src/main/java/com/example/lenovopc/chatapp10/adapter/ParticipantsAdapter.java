package com.example.lenovopc.chatapp10.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.example.lenovopc.chatapp10.R;
import com.example.lenovopc.chatapp10.app.MyApplication;
import com.example.lenovopc.chatapp10.helper.MyPreferenceManager;
import com.example.lenovopc.chatapp10.model.ChatRoom;
import com.example.lenovopc.chatapp10.model.ContactsRoom;
import com.example.lenovopc.chatapp10.model.ParticipantsRoom;


public class ParticipantsAdapter extends RecyclerView.Adapter<ParticipantsAdapter.ViewHolder> {

    private Context mContext;
    ArrayList checked = new ArrayList();

    private ArrayList<ParticipantsRoom> listTestParticipants;
    //   private static String today;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView name, mobile;
        public CheckBox chkSelected;
        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            mobile = (TextView) view.findViewById(R.id.mobile);
            chkSelected = (CheckBox) view
                    .findViewById(R.id.chkSelected);
            // timestamp = (TextView) view.findViewById(R.id.timestamp);
            //count = (TextView) view.findViewById(R.id.count);
        }
    }


    public ParticipantsAdapter(Context mContext, ArrayList<ParticipantsRoom> listTestParticipants) {
        this.mContext = mContext;
        this.listTestParticipants = listTestParticipants;

        Calendar calendar = Calendar.getInstance();
        //    today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.participants_room_list_row, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        ParticipantsRoom participantsRoom = listTestParticipants.get(position);
        holder.name.setText(participantsRoom.getName());
        holder.mobile.setText(participantsRoom.getMobile());
        holder.chkSelected.setChecked(listTestParticipants.get(position).isSelected());
        holder.chkSelected.setTag(listTestParticipants.get(position));


        holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                ParticipantsRoom par = (ParticipantsRoom) cb.getTag();
                par.setSelected(cb.isChecked());
              // for(int i = 0; i < cb.length(); i++) {
               // checked.add(par.getId().toString());
                   if (cb.isChecked() == true) {

                       checked.add(par.getId().toString());
                    //   Toast.makeText(v.getContext(), checked.toString(), Toast.LENGTH_SHORT).show();
                   }else {
                       checked.remove(par.getId().toString());

                   }

            //    Toast.makeText(v.getContext(), Arrays.toString(checked.toArray()), Toast.LENGTH_SHORT).show();

                listTestParticipants.get(position).setSelected(cb.isChecked());
                String memberString = Arrays.toString(checked.toArray());
                MyApplication.getInstance().getPrefManager().setMemberString(memberString.substring(1,memberString.length()-1));
               // par.setMemberString(memberString);


             /*  Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: " + cb.getText() + " is "+position+""
                                + cb.isChecked(), Toast.LENGTH_LONG).show();*/
            }
        });

   /*     holder.message.setText(contactRoom.getLastMessage());
        if (chatRoom.getUnreadCount() > 0) {
            holder.count.setText(String.valueOf(contactRoom.getUnreadCount()));
            holder.count.setVisibility(View.VISIBLE);
        } else {
            holder.count.setVisibility(View.GONE);
        }

        holder.timestamp.setText(getTimeStamp(chatRoom.getTimestamp()));*/
    }

    @Override
    public int getItemCount() {
        return listTestParticipants.size();
    }
    /*
        public static String getTimeStamp(String dateStr) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String timestamp = "";

            today = today.length() < 2 ? "0" + today : today;

            try {
                Date date = format.parse(dateStr);
                SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
                String dateToday = todayFormat.format(date);
                format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
                String date1 = format.format(date);
                timestamp = date1.toString();
            } catch (ParseException e) {
                e.printStackTrace();
            }

            return timestamp;
        }
    */
    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ParticipantsAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ParticipantsAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
