package com.example.lenovopc.chatapp10.activity;

import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.TextInputLayout;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;


import com.android.volley.toolbox.Volley;
import com.example.lenovopc.chatapp10.Manifest;
import com.example.lenovopc.chatapp10.R;
import com.example.lenovopc.chatapp10.app.Config;
import com.example.lenovopc.chatapp10.app.EndPoints;
import com.example.lenovopc.chatapp10.app.MyApplication;
import com.example.lenovopc.chatapp10.model.ContactsRoom;
import com.example.lenovopc.chatapp10.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



public class LoginActivity extends AppCompatActivity {

    private String TAG = LoginActivity.class.getSimpleName();
    private EditText inputName, inputEmail, inputMobile;
    private TextInputLayout inputLayoutName, inputLayoutEmail;
    private Button btnEnter;
    private String READ_CONTACTS;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 123;
    ArrayList<ContactsRoom> listTest1;
    ArrayList listTest2 = new ArrayList<HashMap>();
    private SQLiteDatabase db;
    private static final String SELECT_N = "SELECT * FROM NoticeBoardContacts";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

//        int permissionCheck = ContextCompat.checkSelfPermission(LoginActivity.this,android.Manifest.permission.READ_CONTACTS);
//
//        if(permissionCheck != PackageManager.PERMISSION_GRANTED){
//
//            ActivityCompat.requestPermissions(LoginActivity.this,
//                    new String[]{android.Manifest.permission.READ_CONTACTS},
//                    MY_PERMISSIONS_REQUEST_READ_CONTACTS);
//            return;
//
//        }

        /**
         * Check for login session. It user is already logged in
         * redirect him to main activity
         * */
        if (MyApplication.getInstance().getPrefManager().getUser() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        inputLayoutEmail = (TextInputLayout) findViewById(R.id.input_layout_email);
        inputName = (EditText) findViewById(R.id.input_name);
        inputEmail = (EditText) findViewById(R.id.input_email);
        inputMobile = (EditText) findViewById(R.id.inputMobile);
        btnEnter = (Button) findViewById(R.id.btn_enter);

        inputName.addTextChangedListener(new MyTextWatcher(inputName));
        inputEmail.addTextChangedListener(new MyTextWatcher(inputEmail));

        btnEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    /**
     * logging in user. Will make http post request with name, email
     * as parameters
     */



    private void login() {
        if (!validateName()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }

        final String name = inputName.getText().toString();
        final String email = inputEmail.getText().toString();
        final String mobile = inputMobile.getText().toString();

        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoints.LOGIN, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error flag
           if (obj.getString("error").equals("false")) {
                        // user successfully logged in

                        JSONObject userObj = obj.getJSONObject("user");
                        User user = new User(userObj.getString("user_id"),
                                userObj.getString("name"),
                                userObj.getString("email"),
                                userObj.getString("mobile"));

                        // storing user in shared preferences
                        MyApplication.getInstance().getPrefManager().storeUser(user);

                        // start main activity
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();

             } else {
                        // login error - simply toast the message
                        Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("email", email);
                params.put("mobile", mobile);

                return params;
            }
        };

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);






    }

    public void fetchContacts() {



        String phoneNumber = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        StringBuffer output = new StringBuffer();

        ContentResolver contentResolver = getContentResolver();

        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0) {

                    output.append("\n First Name:" + name);

                    // Query and loop for every phone number of the contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);

                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        output.append("\n Phone number:" + phoneNumber.trim());

                        //add validation for mobile no. digits to be 10+
                        String trimmed = phoneNumber.replaceAll("\\s+", "");
                        if (trimmed.length() >= 10) {
                            HashMap hashMap = new HashMap();
                            hashMap.put("mobile", trimmed);
                            hashMap.put("name", name);
                            listTest2.add(hashMap);
                        }

                    }

                    phoneCursor.close();

                }

                output.append("\n");
            }

        }
    }


    public void getNoticeBoardContacts() {

        fetchContacts();

        String url = Config.DATA_URL;
        final String inp;
        JSONArray jsonArray = new JSONArray(listTest2);
        inp = jsonArray.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //  loading.dismiss();
                showJSON(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(MyContacts.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("inp", inp);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void showJSON(String response){

        String query = "INSERT INTO NoticeBoardContacts (mobile,name,user_id) VALUES";

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
            for (int i = 0; i < result.length(); i++) {
                JSONObject contactsData = result.getJSONObject(i);

                final ContactsRoom cr1 = new ContactsRoom();
                cr1.setMobile(contactsData.getString("mobile"));
                cr1.setName(contactsData.getString("name"));
                cr1.setId(contactsData.getString("user_id"));
                listTest1.add(cr1);

                ArrayList al = getDBContacts();
                if (al.contains(contactsData.getString("mobile"))) {
                    Log.d("inactivity:", "No insertion");
                } else {
                    query += " ('" + contactsData.getString("mobile") + "', '" + contactsData.getString("name") + "', '" + contactsData.getString("user_id") + "')";
                    if (i < result.length() - 1) {
                        query += ",";
                    } else {
                        query += ";";
                    }
                }
//                if (getDBContacts().) {
//                    }
//                } else {
//                    query += " ('" + contactsData.getString("mobile") + "', '" + contactsData.getString("user_id") + "')";
//                    if (i < result.length() - 1) {
//                        query += ",";
//                    } else {
//                        query += ";";
//                    }
//                }
            }
            try {
                db.execSQL(query);
            } catch (SQLException e) {
                Log.d("error: ", "error");
            } finally {
                Log.d("status: ", "inserted");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    //    mAdapterContacts.notifyDataSetChanged();
    }


    public ArrayList getDBContacts() {

        ArrayList dbContacts = new ArrayList();
        Cursor c;
        c = db.rawQuery(SELECT_N, null);
        c.moveToFirst();

        while (!c.isLast() && c.getCount() > 0) {
            dbContacts.add(c.getString(c.getColumnIndex("mobile")));
            c.moveToNext();
        }

//        String dbString = Arrays.toString(dbContacts.toArray());
//        return dbString.substring(1, dbString.length() - 1);
        return dbContacts;
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    // Validating name
    private boolean validateName() {
        if (inputName.getText().toString().trim().isEmpty()) {
            inputLayoutName.setError(getString(R.string.err_msg_name));
            requestFocus(inputName);
            return false;
        } else {
            inputLayoutName.setErrorEnabled(false);
        }

        return true;
    }

    // Validating email
    private boolean validateEmail() {
        String email = inputEmail.getText().toString().trim();

        if (email.isEmpty() || !isValidEmail(email)) {
            inputLayoutEmail.setError(getString(R.string.err_msg_email));
            requestFocus(inputEmail);
            return false;
        } else {
            inputLayoutEmail.setErrorEnabled(false);
        }

        return true;
    }

    private static boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_email:
                    validateEmail();
                    break;
            }
        }
    }
}

