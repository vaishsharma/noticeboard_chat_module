package com.example.lenovopc.chatapp10.model;


import android.graphics.Bitmap;

import java.io.Serializable;

public class ParticipantsRoom implements Serializable {
    String name, mobile, user_id, memberString;
    Bitmap photo;
    private boolean isSelected;

    public ParticipantsRoom() {

    }
    public String getId(){
        return user_id;
    }

    public void setId(String user_id){

        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    public Bitmap getPhoto() {
        return photo;
    }

    public void setPhoto(Bitmap photo) {
        this.photo = photo;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public String getMemberString(){
        return memberString;
    }

    public void setMemberString(String memberString){
        this.memberString = memberString;
    }

}