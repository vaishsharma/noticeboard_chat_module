package com.example.lenovopc.chatapp10.activity;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovopc.chatapp10.R;
import com.example.lenovopc.chatapp10.adapter.ContactsAdapter;
import com.example.lenovopc.chatapp10.adapter.ParticipantsAdapter;
import com.example.lenovopc.chatapp10.app.MyApplication;
import com.example.lenovopc.chatapp10.helper.SimpleDividerItemDecoration;
import com.example.lenovopc.chatapp10.model.ChatRoom;
import com.example.lenovopc.chatapp10.model.ContactsRoom;
import com.example.lenovopc.chatapp10.model.ParticipantsRoom;
import com.example.lenovopc.chatapp10.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CreateChatRoom extends AppCompatActivity implements View.OnClickListener {

    private static final String REGISTER_URL = "http://192.168.1.11/gcm_chat/create_ChatRoom.php";

    public static final String KEY_NAME = "name";

    User user;
    private EditText editTextChatRoomName;
    private Button buttonBack;
    private RecyclerView recyclerViewParticipants;
    ArrayList<ParticipantsRoom> listTestParticipants;
    private ParticipantsAdapter mAdapterParticipants;

    private Button buttonCreateChatRooms;

    Boolean showRoom;

    private static final String SELECT_SQL = "SELECT * FROM NoticeBoardContacts";

    private SQLiteDatabase db;

    private Cursor c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_chat_room);

        openDatabase();

        showRoom = getIntent().getBooleanExtra("show_chat_room",false);
        editTextChatRoomName = (EditText) findViewById(R.id.editTextChatRoomName);

        buttonCreateChatRooms = (Button) findViewById(R.id.buttonCreateChatRoom);
        buttonCreateChatRooms.setOnClickListener(this);

      //  buttonBack = (Button) findViewById(R.id.buttonBack);
//        buttonBack.setOnClickListener(this);

        recyclerViewParticipants = (RecyclerView) findViewById(R.id.recycler_view_participants);

        listTestParticipants = new ArrayList<>();
        mAdapterParticipants = new ParticipantsAdapter(this, listTestParticipants);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewParticipants.setLayoutManager(layoutManager);
        recyclerViewParticipants.addItemDecoration(new SimpleDividerItemDecoration(
                getApplicationContext()
        ));
        recyclerViewParticipants.setItemAnimator(new DefaultItemAnimator());
        recyclerViewParticipants.setAdapter(mAdapterParticipants);

        c = db.rawQuery(SELECT_SQL, null);
        c.moveToFirst();
        showRecords();

     //  listTestParticipants.clear();

    }


    private void createChatRooms() {
        user = MyApplication.getInstance().getPrefManager().getUser();
        final ParticipantsRoom prr = new ParticipantsRoom();
        final String name = editTextChatRoomName.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            editTextChatRoomName.setError("Please enter name for chat room");
            return;
        } else {
            StringRequest stringRequest = new StringRequest(Request.Method.POST, REGISTER_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (Integer.parseInt(jsonObject.getString("success")) == 1) {

                                    Intent intent;
                                    if(showRoom) {
                                        ChatRoom cr = new ChatRoom();
                                        cr.setId(jsonObject.getString("chat_room_id"));
                                        cr.setName(name);
                                        cr.setLastMessage("");
                                        cr.setUnreadCount(0);
                                        cr.setTimestamp(jsonObject.getString("created_at"));

                                        intent = new Intent(CreateChatRoom.this, ChatRoomActivity.class);
                                        intent.putExtra("chat_room_id", cr.getId());
                                        intent.putExtra("name", cr.getName());
                                        finish();
                                        startActivity(intent);
                                    }else {
                                        intent = new Intent(CreateChatRoom.this, MainActivity.class);
                                        finish();
                                        startActivity(intent);
                                    }
                                }
                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
//                            Toast.makeText(CreateChatRoom.this, response, Toast.LENGTH_LONG).show();
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Toast.makeText(CreateChatRoom.this, error.toString(), Toast.LENGTH_LONG).show();
                        }
                    }) {
                @Override
                protected Map<String, String> getParams() {
                    Map<String, String> params = new HashMap<>();
                    params.put(KEY_NAME, name);
                    params.put("user_id", user.getId());
                    params.put("member_id",MyApplication.getInstance().getPrefManager().getMemberString());

                    return params;
                }

            };

            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }


    private void back(){
        Intent i2 = new Intent(CreateChatRoom.this, MainActivity.class);
        startActivity(i2);

    }


        protected void openDatabase() {
        db = openOrCreateDatabase("NoticeBoardDB", Context.MODE_PRIVATE, null);
    }

        protected void showRecords() {

//         listTestParticipants.clear();
            while(!c.isLast() && c.getCount() > 0) {
                final ParticipantsRoom pr = new ParticipantsRoom();
                String name = c.getString(c.getColumnIndex("name"));
                String mobile = c.getString(c.getColumnIndex("mobile"));
                String user_id = c.getString(c.getColumnIndex("user_id"));
                pr.setMobile(mobile);
                pr.setName(name);
                pr.setId(user_id);
                listTestParticipants.add(pr);
                c.moveToNext();
            }

        }


        protected void moveNext() {
        if (!c.isLast())
            c.moveToNext();

        showRecords();
    }

        protected void movePrev() {
        if (!c.isFirst())
            c.moveToPrevious();

        showRecords();

    }





        @Override
        public void onClick (View v){
            if (v == buttonCreateChatRooms) {
                createChatRooms();
            }
          /*    else  if (v == buttonBack) {
                   back();

            }*/
        }
    }
