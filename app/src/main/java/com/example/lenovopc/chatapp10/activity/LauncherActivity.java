package com.example.lenovopc.chatapp10.activity;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.lenovopc.chatapp10.R;
import com.example.lenovopc.chatapp10.app.Config;
import com.example.lenovopc.chatapp10.model.ContactsRoom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LauncherActivity extends AppCompatActivity {

    ArrayList<ContactsRoom> listTest1 = new ArrayList<>();
    ArrayList listTest2 = new ArrayList<HashMap>();
    private SQLiteDatabase db;
    private static final String SELECT_N = "SELECT * FROM NoticeBoardContacts";
    private ProgressBar spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        spinner=(ProgressBar)findViewById(R.id.progressBar);

        createDatabase();
        getNoticeBoardContacts();
    }


    protected void createDatabase(){
        spinner.setVisibility(View.VISIBLE);

        db=openOrCreateDatabase("NoticeBoardDB", Context.MODE_PRIVATE, null);
        db.execSQL("CREATE TABLE IF NOT EXISTS NoticeboardContacts(mobile VARCHAR, name VARCHAR, user_id VARCHAR);");
        spinner.setVisibility(View.GONE);

    }


    //Getting contacts from phone book
    public void fetchContacts() {



        String phoneNumber = null;

        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        StringBuffer output = new StringBuffer();

        ContentResolver contentResolver = getContentResolver();

        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);

        // Loop for every contact in the phone
        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));

                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));

                if (hasPhoneNumber > 0) {

                    output.append("\n First Name:" + name);

                    // Query and loop for every phone number of the contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);

                    while (phoneCursor.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        output.append("\n Phone number:" + phoneNumber.trim());

                        //add validation for mobile no. digits to be 10+
                        String trimmed = phoneNumber.replaceAll("\\s+", "");
                        if (trimmed.length() >= 10) {
                            HashMap hashMap = new HashMap();
                            hashMap.put("mobile", trimmed);
                            hashMap.put("name", name);
                            listTest2.add(hashMap);
                        }

                    }

                    phoneCursor.close();

                }

                output.append("\n");
            }

        }
    }


    public void getNoticeBoardContacts() {
        spinner.setVisibility(View.VISIBLE);

        fetchContacts();

        String url = Config.DATA_URL;
        final String inp;
        JSONArray jsonArray = new JSONArray(listTest2);
        inp = jsonArray.toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //  loading.dismiss();
                showJSON(response);
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
//                            Toast.makeText(MyContacts.this, error.getMessage().toString(), Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("inp", inp);

                return params;
            }
        };

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }

    private void showJSON(String response){

        String query = "INSERT INTO NoticeBoardContacts (mobile,name,user_id) VALUES";

        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray result = jsonObject.getJSONArray(Config.JSON_ARRAY);
            for (int i = 0; i < result.length(); i++) {
                JSONObject contactsData = result.getJSONObject(i);

                final ContactsRoom cr1 = new ContactsRoom();
                cr1.setMobile(contactsData.getString("mobile"));
                cr1.setName(contactsData.getString("name"));
                cr1.setId(contactsData.getString("user_id"));
                listTest1.add(cr1);

                ArrayList al = getDBContacts();
                if (al.contains(contactsData.getString("mobile"))) {
                    Log.d("inactivity:", "No insertion");
                } else {
                    query += " ('" + contactsData.getString("mobile") + "', '" + contactsData.getString("name") + "', '" + contactsData.getString("user_id") + "')";
                    if (i < result.length() - 1) {
                        query += ",";
                    } else {
                        query += ";";
                    }
                }
//                if (getDBContacts().) {
//                    }
//                } else {
//                    query += " ('" + contactsData.getString("mobile") + "', '" + contactsData.getString("user_id") + "')";
//                    if (i < result.length() - 1) {
//                        query += ",";
//                    } else {
//                        query += ";";
//                    }
//                }
            }
            try {
                db.execSQL(query);
            } catch (SQLException e) {
                Log.d("error: ", "error");
            } finally {
                Log.d("status: ", "inserted");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Intent sat = new Intent(LauncherActivity.this,LoginActivity.class);
        startActivity(sat);
        spinner.setVisibility(View.GONE);
        finish();
        //    mAdapterContacts.notifyDataSetChanged();
    }


    public ArrayList getDBContacts() {

        ArrayList dbContacts = new ArrayList();
        Cursor c;
        c = db.rawQuery(SELECT_N, null);
        c.moveToFirst();

        while (!c.isLast() && c.getCount() > 0) {
            dbContacts.add(c.getString(c.getColumnIndex("mobile")));
            c.moveToNext();
        }

//        String dbString = Arrays.toString(dbContacts.toArray());
//        return dbString.substring(1, dbString.length() - 1);
        return dbContacts;
    }


}

