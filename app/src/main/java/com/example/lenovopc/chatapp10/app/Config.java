package com.example.lenovopc.chatapp10.app;

public class Config {

    // flag to identify whether to show single line
    // or multi line text in push notification tray
    public static boolean appendNotificationMessages = true;

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // type of push messages
    public static final int PUSH_TYPE_CHATROOM = 1;
    public static final int PUSH_TYPE_USER = 2;

    // id to handle the notification in the notification try
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String DATA_URL = "http://192.168.1.11/gcm_chat/checkContacts.php";
    public static final String JSON_ARRAY= "result";
    public static final String KEY_MOBILE= "mobile";
    public static final String URL_DELETE_CHATROOM = "http://192.168.1.11/gcm_chat/delete_chat_room.php?id=";
}